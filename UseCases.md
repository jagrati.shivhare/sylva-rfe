# Use cases with Business Needs and High level requirements

This Document describes the Use cases and the associated Technical requirements. This list help to build the SylvaStack roadmap.

## 5G Core
* CNFs of the 5GCore : UPF, SDM, UDM ....
* Requirements : Hugepages, SRIOV, DPDK
* Requirements covered by the release V0.3 of SylvaStack

## NaaS (include CAMARA API integration)
* Test the integration with CAMARA API.
* No specific technical Requirement identified yet
* Requirements covered by the release V0.3 of SylvaStack

## Edge Federation (interlock the edge Federation solution with the Telco Cloud Layer)
* Test the integration with CAMARA API.
* No specific technical Requirement identified yet

## Edge VR (support of GPU)
* support of GPU Acceleration
* Test the integration of VR

## Tenant isolation optimizing baremetal resources
* hard multi-tenancy (separate cluster for each tenant) optimising the usage of baremetal infrastructure
* Technical requirements: support of Sylva deployment over KubeVirt or other tools to manage microVM over K8s

## SDWAN
* Support SDWAN Network functions
* Tec Requirements : Kubevirt, specific network attachments

## LAN Automation
* Important features for some DU ORAN Vendors to manage high volume of VLANs 
* From Declarative configuration in the Sylva management cluster to provision VLAN in L2 Switches
* Perhaps interlock with Nephio Network Management

## CU/DU O-RAN
* Support CU & DU CNF
* Technical Requirements : RT OS. PTP best design (PTP Client provisioning).

## PaaS for CNF LCM (Service Mesh, …)
* Tools to manage the LCM of CNF
* Service Mesh, egress control
* CNF Log & Monitoring

## IT Convergence (app Isolation)
* IT & Network Convergence
* Technical Requirements : Application Isolation, VCluster or alternative support

## LoadBalancing Private & Public Cloud
* Hybrid Cloud. For example Extend in a Peak of Traffic the private TelcoCloud to hyperscaler.
* Common Management Cluster to manage Private & Public Cloud CaaS.
