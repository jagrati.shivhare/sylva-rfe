# Enhancement/Feature description

To have additional level security for untrusted workload, we would like to propose kata container.

*A lightweight virtual machines that feel and perform like containers, but provide stronger workload isolation  using hardware virtualization technology as a second layer of defense.*



# Technical implementation

We will be needing these components to work with kata containers.

#### Perquisites -

- Kata container must be installed on all nodes.
- Configure runtime class on k8s cluster to make use of kata container.
- Mutating web hook that can add run time class on k8s object such as pod, deployment and so on.

We have developed custom controller using kopf operator that performs following tasks -

- Check for untrusted annotation
- Scan manifest with kubescape ( can be any other tool )
- Add runtimeclass

#### Operator work flow -



<img src="flow_daigram.png" alt="image-20230425164656910" style="zoom:67%;" />

# How to test it

After the implementation, kata runtime class should be dynamically added to the manifest on the basis of annotation and untrusted.

